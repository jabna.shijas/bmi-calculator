const height = document.getElementById("height")
const weight = document.getElementById("weight")
const calculateBtn = document.getElementById("calculate")
const reloadBtn = document.getElementById("reload")
const bmiDisplay = document.getElementById("bmi");
const categoryDisplay = document.getElementById("category");


function getHeightinMeters() {
  const heightValue = height.value;
  return heightValue * 0.3048;
}


function calculate() {
  const heightMeter = getHeightinMeters();

  const weightValue = weight.value
  
  if (isNaN(heightMeter) || isNaN(weightValue)) {
    alert("Please enter valid numbers for height and weight.");
    return;
}
  const bmi = (weightValue / (heightMeter * heightMeter)).toFixed(2);
  bmiDisplay.innerHTML = `Your BMI: ${bmi}`;
  Display(bmi)


}
function Display(bmi) {

  if (bmi < 18.5) {
    categoryDisplay.innerHTML = "You are underweight"
  }
  else if (bmi < 25) {
    categoryDisplay.innerHTML = "You are healthyweight"
  }
  else if (bmi < 30) {
    categoryDisplay.innerHTML = "You are overweight"
  }
  else {
    categoryDisplay.innerHTML = "You are obese"
  }

}


function reloadpage() {
  location.reload();
}

reloadBtn.addEventListener("click", reloadpage)
calculateBtn.addEventListener("click", calculate)

